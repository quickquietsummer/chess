import Figure from "./Figure";
import black from "../../assets/images/king-black.png";
import white from "../../assets/images/king-white.png";

export default class King extends Figure {

    imageSourceBlack(): string {
        return black;
    }

    imageSourceWhite(): string {
        return white;
    }
}