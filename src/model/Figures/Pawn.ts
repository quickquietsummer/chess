import Figure from "./Figure";
import black from "../../assets/images/pawn-black.png";
import white from "../../assets/images/pawn-white.png";

export default class Pawn extends Figure {

    imageSourceBlack(): string {
        return black;
    }

    imageSourceWhite(): string {
        return white;
    }
}