import Figure from "./Figure";
import {Team} from "./Team";

export default interface IFigureConstructor {
    new(team: Team): Figure,
}
