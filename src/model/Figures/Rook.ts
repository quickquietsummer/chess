import Figure from "./Figure";
import black from "../../assets/images/rook-black.png";
import white from "../../assets/images/rook-white.png";

export default class Rook extends Figure {

    imageSourceBlack(): string {
        return black;
    }

    imageSourceWhite(): string {
        return white;
    }
}