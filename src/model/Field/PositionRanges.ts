export type PositionRanges = {
    xStart: number,
    xEnd: number,
    yStart: number,
    yEnd: number
}