import StateContext from "./StateContext";

export type Transit = (state: IState) => void

export default interface IState {
    handle(context: StateContext, transit: Transit): void;
}