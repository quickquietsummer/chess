import Figure from "./Figure";
import black from "../../assets/images/queen-black.png";
import white from "../../assets/images/queen-white.png";

export default class Queen extends Figure {

    imageSourceBlack(): string {
        return black;
    }

    imageSourceWhite(): string {
        return white;
    }
}