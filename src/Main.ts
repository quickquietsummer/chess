import Field from "./model/Field/Field";
import Presenter from "./presentation/Presenter";
import {ceil, sqrt} from "mathjs";
import Environment from "./environment";

const cellCount = 64
const cellHeight = Environment.gameHeight / <number>sqrt(cellCount)
const cellWidth = Environment.gameWidth / <number>sqrt(cellCount)

export default class Main {
    private _field: Field | undefined
    private _ctx: CanvasRenderingContext2D | undefined
    private _presenter: Presenter | undefined;

    async init() {
        this._ctx = this.initCanvasContext2d()

        this._field = new Field()
        await this._field.init()

        this._presenter = new Presenter(this._ctx)
        this._presenter.render(this._field.data)

        console.log(this._field.data)
        this._field.onChange = this.onFieldChangeHandler.bind(this)
        this._ctx.canvas.addEventListener('click', this.onClickHandler.bind(this))
    }

    onFieldChangeHandler() {
        console.log('CHANGED')
        if (!this._field) {
            throw new Error("Отсутствует поле field")
        }
        console.log(this._field.data)
        if (!this._presenter) {
            throw new Error("Отсутствует поле presenter")
        }
        this._presenter.render(this._field.data)
    }

    onClickHandler(e: MouseEvent) {
        let x = ceil(e.offsetX / cellWidth) - 1
        let y = ceil(e.offsetY / cellHeight) - 1

        if (!this._field) {
            throw new Error("Отсутствует поле field")
        }
        this._field.touch({x, y})
    }

    initCanvasContext2d(): CanvasRenderingContext2D {
        let canvas: HTMLCanvasElement = document.createElement('canvas');
        canvas.height = Environment.gameHeight;
        canvas.width = Environment.gameWidth;
        canvas.id = "game";
        document.body.appendChild(canvas);
        let ctx = canvas?.getContext('2d');
        if (!ctx) {
            throw new Error('context2d отсутсвует')
        }
        return ctx;
    }


}