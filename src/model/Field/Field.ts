import {FieldData} from "./FieldData";
import {make2DimensionalArray} from "../../utils";
import Initiator from "./Initiator";
import StateContext from "./StateMachine/StateContext";
import StateMachine from "./StateMachine/StateMachine";
import StateIdle from "./StateMachine/States/StateIdle";

export default class Field {
    private readonly _data: FieldData
    private _from: Position | null = null
    private _stateMachine: StateMachine
    private _stateContext: StateContext
    private _onChange: Function | undefined
    set onChange(callback: Function) {
        this._onChange = callback
    }

    get data() {
        return this._data;
    }

    constructor() {
        this._data = make2DimensionalArray(8, 8)
        this._stateContext = new StateContext(this._data, new StateIdle())
        this._stateMachine = new StateMachine(this._stateContext, this.dispatchOnChange.bind(this))
    }


    public async init() {
        let initiator = new Initiator(this._data);
        return initiator.init();
    }

    private dispatchOnChange() {
        if (!!this._onChange) {
            this._onChange()
        }
    }

    public touch(cell: Position) {
        this._stateContext.target = cell;
        this._stateMachine.handle();
    }
}