import {abs,min} from "mathjs";
function vh(percent:number) {
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    return (percent * h) / 100;
}

function vw(percent:number) {
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    return (percent * w) / 100;
}
// abs()
export default class Environment {
    static get gameWidth() {
        // let result = process.env.GAME_WIDTH
        // if (!result) {
        //     throw new Error(this.getNoVariableMessage('GAME_WIDTH'))
        // }
        // return +result;
        return min(vh(100),vw(100))
    }

    static get gameHeight() {
        // let result = process.env.GAME_HEIGHT
        // if (!result) {
        //     throw new Error(this.getNoVariableMessage('GAME_HEIGHT'))
        // }
        // return +result;
        return min(vh(100),vw(100))
    }

    private static getNoVariableMessage(variable: string): string {
        return `Отсутствует переменная окружения ${variable};`
    }
}

