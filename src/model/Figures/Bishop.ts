import Figure from "./Figure";
import black from "../../assets/images/bishop-black.png";
import white from "../../assets/images/bishop-white.png";

export default class Bishop extends Figure {

    imageSourceBlack(): string {
        return black;
    }

    imageSourceWhite(): string {
        return white;
    }
}