import Figure from "./Figure";
import black from "../../assets/images/knight-black.png";
import white from "../../assets/images/knight-white.png";

export default class Knight extends Figure {

    imageSourceBlack(): string {
        return black;
    }

    imageSourceWhite(): string {
        return white;
    }
}