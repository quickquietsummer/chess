export async function createImage(source: string) {
    return new Promise<HTMLImageElement>(function (resolve) {
        console.log(source);

        let image = new Image()

        image.onload = () => {
            resolve(image)
        };

        image.onerror = (event, source, lineno, colno, error) => {
            console.error(error)
        };

        image.src = source
    });
}

export function make2DimensionalArray(length1: number, length2: number): Array<Array<null>> {
    return new Array(length1).fill(null).map(() => new Array(length2).fill(null));
}