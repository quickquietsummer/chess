import IState, {Transit} from "../IState";
import StateContext from "../StateContext";
import StateIdle from "./StateIdle";
import fart from "../../../../assets/sound/fart.mp3";
import skala from "../../../../assets/sound/skala.mp3";
import {randomInt} from "mathjs";

export default class StateChosen implements IState {
    handle(context: StateContext, transit: Transit): void {
        console.log("HANDLE CHOOSED")
        if (this.isCellRepeating(context)) {
            console.error('Одна и таже клетка. Отмена выбора')
            context.from = null;
            this.unchose(context)
            console.info('TRANSIT TO IDLE')
            transit(new StateIdle())
            return
        }
        this.move(context)
        context.from = null;

        console.info('TRANSIT TO IDLE')
        let audios = [fart, skala]
        let randomResult = randomInt(0, audios.length)
        let audio = new Audio(audios[randomResult])
        audio.play()
        transit(new StateIdle())
    }

    private move(context: StateContext) {
        if (!context.from) {
            throw new Error("В контексте отсутствует from")
        }
        if (!context.target) {
            throw new Error("Отсутствует таргет")
        }
        context.fieldData[context.target.x][context.target.y] = context.fieldData[context.from.x][context.from.y];
        this.unchose(context)
        context.fieldData[context.from.x][context.from.y] = null;
    }

    private unchose(context: StateContext) {
        if (!context.target) {
            throw new Error("Отсутствует таргет")
        }
        context.fieldData[context.target.x][context.target.y]?.unchose()
    }

    private isCellRepeating(context: StateContext) {
        if (!context.from) {
            throw new Error("From отсутствует")
        }
        if (!context.target) {
            throw new Error("Target отсутствует")
        }
        let isSameX = context.from.x == context.target.x;
        let isSameY = context.from.y == context.target.y;
        return isSameX && isSameY
    }

}