const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const Dotenv = require('dotenv-webpack');
module.exports = {
    mode: "development",
    entry: './src/index.ts',
    devtool: 'inline-source-map',
    module: {

        rules: [
            {
                test: /\.ts?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                loader: 'file-loader',
                options: {
                    outputPath: "images"
                }
            }, {
                test: /\.mp3$/i,
                loader: 'file-loader',
                options: {
                    outputPath: "audio"
                }
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            esModule: false
                        }
                    },
                    "css-loader",
                    "sass-loader",
                ],
            },
        ]
    },
    plugins: [
        new Dotenv(),
        new MiniCssExtractPlugin(),
    ],
    resolve: {
        extensions: ['.ts', '.js', '.png', 'css', 'scss'],
    },
    output: {
        sourceMapFilename: "sourceMap",
        filename: "index.js",
        path: path.resolve(__dirname, 'public'),
    }
}