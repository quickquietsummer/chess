import IState from "./IState";
import StateContext from "./StateContext";

export default class StateMachine {
    private _context: StateContext
    private _onchange: Function;

    constructor(context: StateContext, onchange: Function) {
        this._context = context;
        this._onchange = onchange;
    }

    public handle() {
        this._context.state.handle(this._context, this.transit.bind(this))
    }

    private transit(state: IState) {
        this._context.state = state;
        this._onchange()
    }
}