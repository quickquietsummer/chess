import Pawn from "../Figures/Pawn";
import {Team} from "../Figures/Team";
import Rook from "../Figures/Rook";
import Queen from "../Figures/Queen";
import King from "../Figures/King";
import Bishop from "../Figures/Bishop";
import Knight from "../Figures/Knight";
import IFigureConstructor from "../Figures/IFigureConstructor";
import {PositionRanges} from "./PositionRanges";
import {FieldData} from "./FieldData";

export default class Initiator {
    private readonly _data: FieldData;

    constructor(data: FieldData) {
        this._data = data;
    }

    public async init() {
        await this.putAll()
    }

    private async putAll() {
        await this.putPawns()
        await this.putRooks()
        await this.putQueens()
        await this.putKings()
        await this.putBishops()
        await this.putKnights()
    }

    private async putPawns() {
        await this.putRanges(
            Pawn,
            Team.White,
            {xStart: 0, xEnd: 7, yStart: 1, yEnd: 1}
        )

        await this.putRanges(
            Pawn,
            Team.Black,
            {xStart: 0, xEnd: 7, yStart: 6, yEnd: 6}
        )
    }

    private async putRooks() {
        await this.put(Rook, Team.Black, {x: 7, y: 7})
        await this.put(Rook, Team.Black, {x: 0, y: 7})
        await this.put(Rook, Team.White, {x: 7, y: 0})
        await this.put(Rook, Team.White, {x: 0, y: 0})
    }


    private async putQueens() {
        await this.put(Queen, Team.Black, {x: 4, y: 7})
        await this.put(Queen, Team.White, {x: 3, y: 0})
    }

    private async putKings() {
        await this.put(King, Team.Black, {x: 3, y: 7})
        await this.put(King, Team.White, {x: 4, y: 0})
    }

    private async putBishops() {
        await this.put(Bishop, Team.Black, {x: 2, y: 7})
        await this.put(Bishop, Team.Black, {x: 5, y: 7})
        await this.put(Bishop, Team.White, {x: 2, y: 0})
        await this.put(Bishop, Team.White, {x: 5, y: 0})
    }

    private async putKnights() {
        await this.put(Knight, Team.Black, {x: 1, y: 7})
        await this.put(Knight, Team.Black, {x: 6, y: 7})
        await this.put(Knight, Team.White, {x: 1, y: 0})
        await this.put(Knight, Team.White, {x: 6, y: 0})
    }

    private async putRanges(figureConstructor: IFigureConstructor, team: Team, positionRanges: PositionRanges) {
        for (let x = positionRanges.xStart; x <= positionRanges.xEnd; x++) {
            for (let y = positionRanges.yStart; y <= positionRanges.yEnd; y++) {
                let position: Position = {x, y}
                await this.put(figureConstructor, team, position);
            }
        }
    }

    private async put(figureConstructor: IFigureConstructor, team: Team, position: Position) {
        let figure = new figureConstructor(team)
        await figure.loadImage()
        this._data[position.x][position.y] = figure;
    }
}