import IState, {Transit} from "../IState";
import StateContext from "../StateContext";
import StateChosen from "./StateChosen";

export default class StateIdle implements IState {
    handle(context: StateContext, transit: Transit): void {
        console.log("HANDLE IDLE")
        if (!context.target) {
            throw new Error("Отсутствует таргет или один из параметров")
        }
        if (this.isCellEmpty(context)) {
            console.error('ПУСТАЯ КЛЕТКА')
            return
        }
        context.from = {x: context.target.x, y: context.target.y}
        context.fieldData[context.target.x][context.target.y]?.choose()
        console.info('TRANSIT TO CHOOSED')
        transit(new StateChosen())
    }

    private isCellEmpty(context: StateContext) {
        if (!context.target) {
            throw new Error('Отсутствует таргет')
        }
        return !context.fieldData[context.target.x][context.target.y]
    }
}