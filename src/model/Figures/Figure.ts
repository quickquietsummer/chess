import {createImage} from "../../utils";
import {Team} from "./Team";

export default abstract class Figure {
    private _image: HTMLImageElement | undefined
    protected _team: Team
    private _isChosen: boolean = false


    constructor(team: Team) {
        this._team = team
    }

    get image(): HTMLImageElement | undefined {
        return this._image
    }

    public choose() {
        this._isChosen = true;
    }

    public unchose() {
        this._isChosen = false
    }

    get isChosen() {
        return this._isChosen
    }

    public async loadImage() {
        this._image = await createImage(this.imageSource())
        return this._image;
    }

    private imageSource(): string {
        if (this._team === Team.White) {
            return this.imageSourceWhite()
        } else {
            return this.imageSourceBlack()
        }
    }

    abstract imageSourceWhite(): string

    abstract imageSourceBlack(): string
}