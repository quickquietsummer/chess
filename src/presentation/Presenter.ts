import {FieldData} from "../model/Field/FieldData";
import {sqrt} from "mathjs";
import Figure from "../model/Figures/Figure";
import Environment from "../environment";

const cellCount = 64
const cellHeight = Environment.gameHeight / <number>sqrt(cellCount)
const cellWidth = Environment.gameWidth / <number>sqrt(cellCount)

export default class Presenter {
    private readonly _ctx: CanvasRenderingContext2D;

    constructor(ctx: CanvasRenderingContext2D) {
        this._ctx = ctx;
    }

    public render(data: FieldData) {
        this.renderCellsAll(data);
        this.renderFiguresAll(data)
    }

    private renderFiguresAll(data: FieldData) {
        let xLength = data.length
        for (let x = 0; x < xLength; x++) {
            let yLength = data[x].length
            for (let y = 0; y < yLength; y++) {
                let figure = data[x][y]
                if (!figure) {
                    continue
                }
                this.renderFigure(figure, x, y)
            }
        }
    }

    private renderFigure(figure: Figure, x: number, y: number) {
        let renderX = x * cellWidth;
        let renderY = y * cellHeight;
        if (!figure.image) {
            throw new Error(`Изображение не загрузилось у элемента ${Figure.name} x:${x} y:${y}`)
        }
        this._ctx.drawImage(figure.image, renderX, renderY, cellHeight, cellWidth)
    }

    private renderCellsAll(data: FieldData) {
        for (let x = 0; x < 8; x++) {
            for (let y = 0; y < 8; y++) {
                this.renderCell(x, y, data)
            }
        }
    }

    private renderCell(x: number, y: number, data: FieldData) {
        let needHighlight = data[x][y]?.isChosen
        let renderX = x * cellWidth;
        let renderY = y * cellHeight;
        let cellColor: string
        if (needHighlight) {
            cellColor = "GREEN"
        } else if ((x + y) % 2) {
            cellColor = "WHITE"
        } else {
            cellColor = "GREY"
        }
        this._ctx.fillStyle = cellColor
        this._ctx.fillRect(renderX, renderY, cellHeight, cellWidth)
    }
}