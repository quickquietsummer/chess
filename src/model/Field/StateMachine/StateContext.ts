import {FieldData} from "../FieldData";
import IState from "./IState";

export default class StateContext {
    public fieldData: FieldData
    public state: IState
    public from: Position | null = null
    public target: Position | null = null

    constructor(fieldData: FieldData, state: IState) {
        this.fieldData = fieldData;
        this.state = state;
    }
}